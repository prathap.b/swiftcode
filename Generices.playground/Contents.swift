// create a generic function
func displayData<T>(data: T) {
    print("Generic Function:")
    print("Data Passed:", data)
}


// If we pass String then it will accept it as String
displayData(data: "Swift")


print("\n")
// if we Pass Int it will act as int argumented Function
displayData(data: 5)
