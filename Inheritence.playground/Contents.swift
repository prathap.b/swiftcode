class Employee{
        
    var fullName:String = ""
    var role = ""
    var salary = 0.0
    
    func doWork() -> Void {
        print("Employee is Working")
    }
}

class Manager:Employee{
    
    
    
    
}

let techManager:Manager = Manager()

techManager.fullName = "Jack Dorsey"


print(techManager.fullName)


