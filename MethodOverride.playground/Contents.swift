class Employee{
        
    var fullName:String = ""
    var role = ""
    var salary = 0.0
    
    func doWork() -> Void {
        print("Employee is Working")
    }
}

class Manager:Employee{
    
    override func doWork() {
        super.doWork()
        print("and also Managing the Developer")
    }
    
    
    
    
}

let techManager:Manager = Manager()

techManager.fullName = "Jack Dorsey"

techManager.doWork()
//print(techManager.fullName)
