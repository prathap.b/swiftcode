class Temperature {
  var celsius: Double = 0

  func setTemperature(celsius: Double) {
    self.celsius = celsius
    print("Celsius:", celsius)
  }
}

extension Temperature {

  func convert() {
      let fahrenheit = (celsius * 1.8) + 32
      print("Fahrenheit:", fahrenheit)
  }
}

let temp1 = Temperature()
temp1.setTemperature(celsius: 16)

// access extension method using class object
temp1.convert()
