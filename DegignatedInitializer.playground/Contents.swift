class Person{
    
    var name:String
    var networth:Int?
    var gender:String!
    
    //Degignated Initializer
    init(){
        
        self.name = "None"
    }
    
    
    
    convenience init(_ networth: Int  , _ gender : String) {
        
        self.init() // calling Degignated
        
        self.networth = networth
        self.gender = gender
    }
}
