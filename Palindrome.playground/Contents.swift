func Palindrome(Orginalstr : String) -> Bool {
    
    return Orginalstr.lowercased() == String(Orginalstr.lowercased().reversed())
}

let Orginalstr1 : String = "Malayalam"
let result : Bool = Palindrome(Orginalstr : Orginalstr1)
print(result)


func Fibonacci(nth_Number: Int) -> Int {
    if ((nth_Number == 0)  && (nth_Number == 1)) {
        return nth_Number
    }
    return Fibonacci(nth_Number: nth_Number - 1 ) + Fibonacci(nth_Number: nth_Number - 2)
}

let result1 : Int  = Fibonacci(nth_Number: 10)
print(result1)





