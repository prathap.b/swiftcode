// Repeating Statement

var sum = 0

for counter in 1...100{
        
        sum = sum + counter
}
// print(sum)


for _ in 1...100{
        
        print("12")
    
}

for _ in 1..<100{
    print("ff")
    
}


let names = ["Anna", "Alex", "Brian", "Jack"] // Array of name String

for name in names{
    print(name)
}
