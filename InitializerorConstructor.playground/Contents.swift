class Employee{
        
    var fullName:String = ""
    var role = ""
    var salary = 0.0
    
    init() {
        
    }
    
    
    
    init(_ fullName:String , _ role:String , _ salary:Double) {
        self.fullName = fullName
        self.role = role
        self.salary = salary
    }
    
    func doingWork() -> Void {
        print("Employee is Working")
    }

}

let Employee1:Employee = Employee()

let employee2:Employee = Employee("Jacksparrow" , "Actor" , 100000000)

print(employee2.salary)

